function jogar() {

    if (document.getElementById("pedra").checked == false && document.getElementById("papel").checked == false && document.getElementById("tesoura").checked == false) {

        alert("Selecione algo");
    }

    else {
        let sorteio = Math.floor(Math.random() * 3);
        switch (sorteio) {
            case 0:
                document.getElementById("computador").src = "imagens/computadorpedra.png";
                break;
            case 1:
                document.getElementById("computador").src = "imagens/computadorpapel.png";
                break;
            case 2:
                document.getElementById("computador").src = "imagens/computadortesoura.png";
                break;

        }
        //vencedor
        if ((document.getElementById("pedra").checked == true && sorteio == 0) || (document.getElementById("papel").checked == true && sorteio == 1) || (document.getElementById("tesoura").checked == true && sorteio == 2)) {
            document.getElementById("placar").innerHTML = "<span style='color: gray;'>Empate</span>";
        }
        else if ((document.getElementById("pedra").checked == true && sorteio == 2) || (document.getElementById("papel").checked == true && sorteio == 0) || (document.getElementById("tesoura").checked == true && sorteio == 1)) {
            document.getElementById("placar").innerHTML = "<span style='color: green;'>Jogador venceu</span>";
        }
        else {
            document.getElementById("placar").innerHTML = "<span style='color: red;'>Computador venceu</span>";
        }

    }
}
jogar()

function resetar() {
    document.getElementById("computador").src = "imagens/computador.png"
    document.getElementById("placar").innerHTML = "";


}